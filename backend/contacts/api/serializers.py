from rest_framework.serializers import ModelSerializer

from ..models import Contact, ContactAddress


class ContactSerializer(ModelSerializer):
    class Meta:
        model = Contact
        fields = '__all__'


class ContactAddressSerializer(ModelSerializer):
    class Meta:
        model = ContactAddress
        fields = '__all__'