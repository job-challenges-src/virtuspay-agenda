from rest_framework.viewsets import ModelViewSet
from rest_framework.response import Response
from rest_framework.decorators import action

from .serializers import ContactSerializer, ContactAddressSerializer
from ..models import Contact, ContactAddress


class ContactViewSet(ModelViewSet):
    serializer_class = ContactSerializer
    queryset = Contact.objects.all()

    @action(detail=True, methods=['get'])
    def address(self, request, pk=None):
        contact:Contact = self.get_object()
        address = contact.address.all()
        return Response(ContactAddressSerializer(address, many=True).data)


class ContactAddressViewSet(ModelViewSet):
    serializer_class = ContactAddressSerializer
    queryset = ContactAddress.objects.all()