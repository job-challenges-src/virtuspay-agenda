from django.urls import path, include
from rest_framework.routers import SimpleRouter

from .viewsets import ContactViewSet, ContactAddressViewSet

routes = SimpleRouter()
routes.register(r'contacts', ContactViewSet)
routes.register(r'address', ContactAddressViewSet)

urlpatterns = [
    path('', include(routes.urls))
]

