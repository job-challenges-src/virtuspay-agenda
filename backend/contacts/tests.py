import re
from django.test import TestCase
from django.core.exceptions import ValidationError
from .validators import validate_phone, REGEX_PHONE
# from .models import Contact

class ContactTest(TestCase):

    def setUp(self):
        self.url = 'http://localhost:8000/api/contacts/'
        return super().setUp()

    def test_regex_of_validation_phone_without_9(self):
        assert re.match(REGEX_PHONE, "(44) 44444444") != None

    def test_regex_of_validation_phone_with_9(self):
        assert re.match(REGEX_PHONE, "(44) 944444444") != None
    
    def test_validation_fone(self):
        with self.assertRaisesRegex(ValidationError, 'Invalid phone'):
            invalid_phone = '99999'
            validate_phone(invalid_phone)

    def test_get_contacts(self):
        response = self.client.get(self.url)
        assert response.status_code == 200
        assert response.json() == []

    def test_save_invalid_contact_phone(self):
        data:dict = { "name": "Irineu", "email": "teste@teste.com", "phone": "99999999" }
        response = self.client.post(self.url, data=data)
        assert response.status_code == 400

    def test_save_invalid_contact_email(self):
        data:dict = { "name": "Irineu", "email": "testeste.com", "phone": "(99) 99999999" }
        response = self.client.post(self.url, data=data)
        assert response.status_code == 400
    
    def test_save_new_contact(self):
        data:dict = { "name": "Irineu", "email": "teste@teste.com", "phone": "(99) 99999999" }
        response = self.client.post(self.url, data=data)
        assert response.status_code == 201
            