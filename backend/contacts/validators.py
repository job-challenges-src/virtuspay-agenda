import re
from django.core.exceptions import ValidationError

REGEX_PHONE = r'^\([0-9]{2}\).{1}[9]?[1-9]{8}$'

def validate_phone(fone:str):
    if not re.match(REGEX_PHONE, fone):
        raise ValidationError('Invalid phone')
