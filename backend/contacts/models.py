from django.db import models
from django.utils.translation import gettext_lazy as _

from .validators import validate_phone


class Contact(models.Model):
    name = models.CharField(_("Nome"), max_length=150)
    email = models.EmailField(_("Email"), max_length=254)
    phone = models.CharField(_("Telefone"), max_length=14, validators=[validate_phone])
    created_at = models.DateTimeField(_("Data de inclusão"), auto_now_add=True)


class ContactAddress(models.Model):
    contact = models.ForeignKey("contacts.Contact", related_name="address", verbose_name=_("Contato"), on_delete=models.CASCADE)
    street = models.CharField(_("rua"), max_length=120)
    street_number = models.CharField(_("número da rua"), max_length=50)
    district = models.CharField(_("bairro"), max_length=80)
    city = models.CharField(_("cidade"), max_length=80)
    state = models.CharField(_("estado"), max_length=2)
    complement = models.CharField(_("complemento"), max_length=250, blank=True)
    zip_code = models.CharField(_("CEP"), max_length=10)
