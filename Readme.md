# Desafio VirtusPay

### Desenvolver uma aplicação web chamada "Agenda"

* Usando qualquer framework web Python
* Usando qualquer framework para frontend.

##### Demais requisitos da aplicação em
```bash
 ./docs/requisitos.pdf

```

### A aplicação
A agenda foi desenvolvida separando frontend e backend.

* O Backend roda na versão 3.8 do Python e foi construído com Django==3.2.3 e DRF==3.12.4
* O Frontend roda na versão 14.0.0 do Node e foi construído utililizando VueJs, QuasarJs e AxiosJs,

## Portas

###### Backend http://localhost:8000
###### Frontend http://localhost:8080

##### Features
* Adicionar e editar contatos
* Adicionar endereços para um contato

##### Next updates

* Editar endereços
* Excluir endereços e contatos

____

## Começando

######  Para rodar o projeto só é necessária a instalação do docker e docker-compose.

```bash
    sudo apt install docker docker-compose
```


###### Após a instalação do docker e docker-compose basta rodar o comando
```bash
make up
```
###### e como mágica vai buildar a aplicação e tudo estará funcionado

______
#### Outros comandos

* Para rodar atualizar as migrations do django
```bash
make db-up
```

* Para rodar os testes do backend
```bash
make test
```


# Autor


#### João Filho

![profile](https://avatars.githubusercontent.com/u/40010888?s=260&u=95581d73711be93046e652237f6d9b027e0d58af&v=4 "João filho")

<p>
<em>
System analyst at <a href="http://www.facam-ma.com.br/">Facam</a><img src="https://media.giphy.com/media/fYSnHlufseco8Fh93Z/giphy.gif" width="30">
</em>
</p>

Portfolio [https://joaofilho.dev](http://joaofilho.dev)

[![Twitter: Drummerzzz_auth](https://img.shields.io/twitter/follow/Drummerzzz_auth?style=social)](https://twitter.com/Drummerzzz_auth)
[![Linkedin: joao-filho-drummerzzz](https://img.shields.io/badge/joao-filho-drummerzzz?style=flat-square&logo=Linkedin&logoColor=white&link=https://www.linkedin.com/in/joao-filho-drummerzzz/)](https://www.linkedin.com/in/joao-filho-drummerzzz/)
[![GitHub Thaiane](https://img.shields.io/github/followers/drummerzzz?label=follow&style=social)](https://github.com/drummerzzz)

##### Full stack developer and drummer

<img src="https://media.giphy.com/media/3o6wred0KQ8Vx5fmYE/giphy.gif" width="430"><br>

