up:
	sudo docker-compose up

db-up:
	sudo docker-compose exec backend python backend/manage.py makemigrations;
	sudo docker-compose exec backend python backend/manage.py migrate;

test:
	sudo docker-compose exec backend python backend/manage.py test;